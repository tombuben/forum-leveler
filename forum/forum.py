import math
import random

import discord
from redbot.core import Config
from redbot.core import commands, checks


class Forum(commands.Cog):
    """ Forum levelling cog"""

    def __init__(self, bot):
        super(Forum, self).__init__()

        self.config = Config.get_conf(self, identifier=112263)

        self.bot = bot

        default_guild = {
            "xps": {
                "short": 5,
                "medium": 10,
                "long": 15,
                "extreme": 0,
                "image": 10,
            },
            "lengths": {
                "short": 50,
                "medium": 140,
                "long": 600,
            },
            "levels": {
                "base_xp": 100,
                "xp_increase": 10,
                "max_xp": 500,
            },
            "xp_randomness_percents": 20,
            "colour": "#03a1fc"
        }
        self.valid_names = set()
        for k, v in default_guild.items():
            if not isinstance(v, dict):
                self.valid_names.add(k)
            else:
                self.valid_names.update([f"{k}.{w}" for w in v.keys()])

        self.config.register_guild(**default_guild)

        self.default_member = {
            "xps": 0,
            "total_xps": 0,
            "level": 0,
        }

        self.config.register_member(**self.default_member)

        default_channel = {
            "ignored": False
        }

        self.config.register_channel(**default_channel)

    @commands.command()
    @checks.admin_or_permissions(manage_guild=True)
    async def forum_config(self, ctx):
        """Lists current Forum configuration"""
        values = await self.config.guild(ctx.guild).all()

        message = f"""
        The leveler is currently configured in the following way:
            - `xps.short`: {values["xps"]["short"]} 
            - `xps.medium`: {values["xps"]["medium"]} 
            - `xps.long`: {values["xps"]["long"]} 
            - `xps.extreme`: {values["xps"]["extreme"]} 
            - `xps.image`: {values["xps"]["image"]} 
            - `lengths.short`: {values["lengths"]["short"]} 
            - `lengths.medium`: {values["lengths"]["medium"]} 
            - `lengths.long`: {values["lengths"]["long"]} 
            - `levels.base_xp`: {values["levels"]["base_xp"]} 
            - `levels.xp_increase`: {values["levels"]["xp_increase"]} 
            - `levels.max_xp`: {values["levels"]["max_xp"]} 
            - `xp_randomness_percents`: {values["xp_randomness_percents"]}
            - `colour`: {values['colour']}
        """
        await ctx.send(message)

    @commands.command()
    @checks.admin_or_permissions(manage_guild=True)
    async def forum_config_set(self, ctx, name, value):
        """Updates forum's configuration"""

        try:
            if name != "colour":
                value = int(value)
            else:
                self.parse_colour(value)
        except ValueError:
            if name != "colour":
                await ctx.send("Please provide a integer.")
            else:
                await ctx.send("Please provide a valid HEX color.")
            return

        attr = self.config.guild(ctx.guild)

        try:
            if name not in self.valid_names:
                raise Exception
            if "." in name:
                first, second = name.split(".")
                attr = getattr(getattr(attr, first), second)
            else:
                attr = getattr(attr, name)
        except Exception:
            await ctx.send("Invalid config name.")
            return

        await attr.set(value)

        await ctx.send(f"Value of {name} was set to {value}")

    async def xps_for_level(self, guild, level) -> int:
        guild_config = await self.config.guild(guild).all()

        xps = guild_config["levels"]["base_xp"] + (level - 1) * guild_config["levels"]["xp_increase"]

        return min(xps, guild_config["levels"]["max_xp"])

    def parse_colour(self, c):
        c = c.lstrip("#")

        return tuple(int(c[i:i + 2], 16) for i in (0, 2, 4))

    async def get_colour(self, ctx):
        colour = await self.config.guild(ctx.guild).colour()

        colour = self.parse_colour(colour)

        return discord.Colour.from_rgb(*colour)

    @commands.command()
    async def level(self, ctx, user: discord.Member = None):
        """ Prints the level of the user
        """
        if user:
            member = user
        else:
            member = ctx.author
        guild = ctx.guild

        member_config = await self.config.member(member).all()
        level = member_config["level"]
        xps = member_config["xps"]

        next_xps = await self.xps_for_level(guild, level + 1)

        rank = await self.get_rank(ctx, member)
        position = self.ordinal(rank)
        medal = {1: ":first_place:", 2: ":second_place:", 3: ":third_place:"}.get(rank, "")

        colour = await self.get_colour(ctx)

        embed = discord.Embed(colour=colour)

        text = (f"{member.mention}'s level is **{level}** and they currently have **{xps}** XPs. "
                f"They are in **{position} position** in the Forum's ranking{medal}. "
                f"*{next_xps}* XPs are required to reach the next level.")

        embed.add_field(name="Forum", value=text)

        await ctx.send(embed=embed)

    async def get_leaderboard(self, ctx):
        members = ctx.guild.members

        list_of_members = []

        for member in members:
            if member.bot:
                continue
            member_config = await self.config.member(member).all()

            if not member_config["total_xps"]:
                continue

            list_of_members.append((
                member, member_config["level"], member_config["total_xps"]
            ))

        list_of_members.sort(key=lambda x: (x[1], x[2]), reverse=True)

        list_of_members = [[i + 1] + list(x) for i, x in enumerate(list_of_members)]

        return list_of_members

    async def get_rank(self, ctx, member):
        leaderboard = await self.get_leaderboard(ctx)

        rank = 0
        for rank, m, *_ in leaderboard:
            if member == m:
                break

        return rank

    @commands.command()
    async def leaderboard(self, ctx):
        """ Prints the Forum leaderboard"""
        list_of_members = await self.get_leaderboard(ctx)

        colour = await self.get_colour(ctx)
        embed = discord.Embed(colour=colour)

        message = ""
        for rank, member, level, xps in list_of_members:
            place = f"{rank}."
            if rank == 1:
                place = ":first_place:"
            if rank == 2:
                place = ":second_place:"
            if rank == 3:
                place = ":third_place:"

            message += f"  {place} {member.display_name}: level **{level}**, *{xps}* total XPs\n"

        if not message:
            message = "Nobody received XPs yet."

        embed.add_field(name="Forum Leaderboard", value=message)

        await ctx.send(embed=embed)

    @commands.command()
    async def forum_ignore(self, ctx, channel: discord.TextChannel = None):
        """Toggles ignore of the channel by the leveler"""
        if channel is None:
            channel = ctx.channel

        ignored = await self.config.channel(channel).ignored()

        new_ignored = not ignored

        await self.config.channel(channel).ignored.set(new_ignored)

        if ignored:
            await ctx.send("The channel will now be included in levelling.")
        else:
            await ctx.send("The channel will now be ignored from levelling.")

    @commands.command()
    @checks.admin_or_permissions(manage_guild=True)
    async def forum_reset(self, ctx, user: discord.Member = None):
        """ Resets level and XPs of all users (or a specific user)
        """
        if user is None:
            users = ctx.guild.members
        else:
            users = [user]

        for user in users:
            for k, v in self.default_member.items():
                await getattr(self.config.member(user), k).set(v)

        if user:
            await ctx.send(f"The progress of {user.display_name} has been reset.")
        else:
            await ctx.send("The progress of all users has been reset.")

    def ordinal(self, n):
        return "%d%s" % (n, "tsnrhtdd"[(n / 10 % 10 != 1) * (n % 10 < 4) * n % 10::4])

    async def listener(self, message: discord.Message):
        if type(message.author) != discord.Member:
            # throws an error when webhooks talk, this fixes it
            return
        if type(message.channel) != discord.channel.TextChannel:
            return
        if message.author.bot:
            return

        if message.content and message.content[0] in await self.bot.get_prefix(message):
            return

        if await self.config.channel(message.channel).ignored():
            return

        guild = message.guild
        config = await self.config.guild(guild).all()

        content = message.content
        content_length = len(content)

        if content_length == 0:
            base_xps = 0
        else:
            for category in ["short", "medium", "long"]:
                if content_length <= config["lengths"][category]:
                    message_category = category
                    break
            else:
                message_category = "extreme"

            base_xps = config["xps"][message_category]

        # image adds more XPs
        if len(message.attachments) or len(message.embeds):
            base_xps += config["xps"]["image"]

        percents = config["xp_randomness_percents"]

        xps_added = math.ceil(base_xps * random.uniform(1 - percents / 100, 1 + percents / 100))

        member = message.author

        member_config = await self.config.member(member).all()

        new_level = level = member_config["level"]
        new_xps = member_config["xps"] + xps_added
        new_total_xps = member_config["total_xps"] + xps_added

        next_xps = await self.xps_for_level(guild, level + 1)

        if new_xps > next_xps:
            new_level += 1
            new_xps -= new_xps
            new_xps = int(new_xps)

        await self.config.member(member).xps.set(new_xps)
        await self.config.member(member).level.set(new_level)
        await self.config.member(member).total_xps.set(new_total_xps)

        if level != new_level:
            next_xps = await self.xps_for_level(guild, new_level + 1)

            colour = await self.get_colour(message)

            embed = discord.Embed(colour=colour)

            rank = await self.get_rank(message, member)
            position = self.ordinal(rank)
            medal = {1: ":first_place:", 2: ":second_place:", 3: ":third_place:"}.get(rank, "")

            text = (f"{member.mention} just advanced to **level {new_level}**. "
                    f"They are in **{position} position** in the Forum's ranking{medal}. "
                    f"*{next_xps}* XPs are needed for the next level.")

            embed.add_field(name="Forum Level Advancement!", value=text)

            await message.channel.send(embed=embed)
